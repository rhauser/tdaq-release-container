# TDAQ Release container

This builds a docker container with a full TDAQ release and 
the necessary LCG packages.

    docker build . -t tdaq:10.1.0

The container size is > 13GByte.

It is available from

    docker pull registry.cern.ch/atlas-tdaq/tdaq:10.1.0

