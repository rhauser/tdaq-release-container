FROM gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-centos7

# AYUM and latest repositories
RUN  git clone https://gitlab.cern.ch/atlas-sit/ayum.git /opt/ayum
COPY yum.repos.d/*.repo /opt/ayum/etc/yum.repos.d/
COPY yum.conf           /opt/ayum/yum.conf

# We need python3 for the LCG post-install scripts
RUN  yum install -y python3 && yum clean all

# Install TDAQ
RUN  /opt/ayum/ayum install -y tdaq-10-01-00_x86_64-centos7-gcc11-opt gcc_11.3.0_x86_64_centos7 LCG_103_pyasn1_0.4.5_x86_64_centos7_gcc11_opt LCG_103_pyasn1_modules_0.2.5_x86_64_centos7_gcc11_opt && /opt/ayum/ayum clean all && rm -rf /opt/ayum/.yumcache

# Install CMake and set env var that cm_setup.sh respects
RUN  mkdir -p /opt/cmake/
RUN  curl -L https://github.com/Kitware/CMake/releases/download/v3.25.1/cmake-3.25.1-linux-x86_64.tar.gz | tar -C /opt/cmake -zxf -
ENV  CMAKE_PATH=/opt/cmake/cmake-3.25.1-linux-x86_64/bin

# Make the only release the default for cm_setup.sh
RUN  cd /sw/atlas/tdaq && ln -s tdaq-10-01-00 prod

